package mx.com.ids.directory.datos;

import java.util.List;

import mx.com.ids.directory.negocio.Colaborador;

public interface ColaboradorData {
	public void run();
	public List<Colaborador> findAll();
	public void findById(Long ide);
	public void deleteById(Long ide);
	public void deleteAll();
	public void existsById(Long ide);
	public void update(String name, String alias, String title, String email, 
			String mobile,String phone, String office, String department_id, String w_a);
}
