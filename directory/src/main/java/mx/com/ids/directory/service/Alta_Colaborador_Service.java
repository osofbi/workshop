package mx.com.ids.directory.service;

import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.com.ids.directory.AppConfig;
import mx.com.ids.directory.datos.ColaboradorData;
import mx.com.ids.directory.negocio.Colaborador;


@RestController
public class Alta_Colaborador_Service {

	@RequestMapping ("/todos")
	public List<Colaborador> todos() {
		
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
		ColaboradorData service = context.getBean(ColaboradorData.class);
		
		service.run();
		List<Colaborador> c= service.findAll();
		
		EntityManagerFactory emf = context.getBean(EntityManagerFactory.class);
		emf.close();
		
		context.close();
		
		/*return "Se dio de alta Colaborador [name=" + name + ", alias=" + alias + ", title=" + title + ", email=" + email + ", mobile="
				+ mobile + ", phone=" + phone + ", office=" + office + ", department=" + department_id + ", w_a=" + w_a
				+ "]";*/
		return c;
	}

}