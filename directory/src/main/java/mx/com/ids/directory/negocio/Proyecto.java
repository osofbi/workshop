package mx.com.ids.directory.negocio;

import java.util.ArrayList;
import java.util.List;

public class Proyecto {
	private String proyect_id;
	private String nombre_proy;
	private String fecha_inicio;
	private String fecha_fin;
	private String nombre_client;
	private List<Colaborador> colaboradores_p=new ArrayList<Colaborador>();
	public String getProyect_id() {
		return proyect_id;
	}
	public void setProyect_id(String proyect_id) {
		this.proyect_id = proyect_id;
	}
	public String getNombre_proy() {
		return nombre_proy;
	}
	public void setNombre_proy(String nombre_proy) {
		this.nombre_proy = nombre_proy;
	}
	public String getFecha_inicio() {
		return fecha_inicio;
	}
	public void setFecha_inicio(String fecha_inicio) {
		this.fecha_inicio = fecha_inicio;
	}
	public String getFecha_fin() {
		return fecha_fin;
	}
	public void setFecha_fin(String fecha_fin) {
		this.fecha_fin = fecha_fin;
	}
	public String getNombre_client() {
		return nombre_client;
	}
	public void setNombre_client(String nombre_client) {
		this.nombre_client = nombre_client;
	}
	public List<Colaborador> getColaboradores_p() {
		return colaboradores_p;
	}
	public void setColaboradores_p(List<Colaborador> colaboradores_p) {
		this.colaboradores_p = colaboradores_p;
	}
	@Override
	public String toString() {
		return "Proyecto [proyect_id=" + proyect_id + ", nombre_proy=" + nombre_proy + ", fecha_inicio=" + fecha_inicio
				+ ", fecha_fin=" + fecha_fin + ", nombre_client=" + nombre_client + ", colaboradores_p="
				+ colaboradores_p + "]";
	}
	public Proyecto() {
		super();
		this.proyect_id = "no tiene";
		this.nombre_proy = "no tiene";
		this.fecha_inicio = "no tiene";
		this.fecha_fin = "no tiene";
		this.nombre_client = "no tiene";
	}
	
	

}
