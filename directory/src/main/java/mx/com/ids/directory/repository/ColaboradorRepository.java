package mx.com.ids.directory.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import mx.com.ids.directory.negocio.Colaborador;


public interface ColaboradorRepository 
	extends CrudRepository<Colaborador, Long> ,JpaRepository<Colaborador, Long>{

}
