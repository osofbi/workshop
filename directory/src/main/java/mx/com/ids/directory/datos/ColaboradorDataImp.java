package mx.com.ids.directory.datos;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.ids.directory.negocio.Colaborador;
import mx.com.ids.directory.repository.ColaboradorRepository;

@Service
public class ColaboradorDataImp implements ColaboradorData {
	@Autowired
	private ColaboradorRepository repository;

	@Override
	public void run() {
		List<Colaborador> Colaboradores = crearColaboradores();
		System.out.println(" -- Saving Colaboradores --");
        repository.saveAll(Colaboradores);
	}

	private List<Colaborador> crearColaboradores() {
		return Arrays.asList(
				Colaborador.create("Rene", "Rene_v", "Coordinador", "Rene.Valentin@ids.com.mx", 
						"5532648519", "12039458", "Ids Insurgentes", "4", "Av insurgentes sur 1318,03230,Mexico DF"),
				Colaborador.create("Luis", "Luis.784", "Coordinador", "Luis.784@ids.com.mx", 
						"5509234414", "56294902", "Ids Insurgentes", "4", "Av insurgentes sur 1318,03230,Mexico DF"),
				Colaborador.create("Jesus", "esus.123", "Coordinador", "Jesus.123@ids.com.mx", 
						"5592384659", "42648920", "Ids Insurgentes", "4", "Av insurgentes sur 1318,03230,Mexico DF"),
				Colaborador.create("Sandra", "Sandra_G", "Coordinador", "Sandra_G@ids.com.mx", 
						"5503728953", "39402785", "Ids Insurgentes", "4", "Av insurgentes sur 1318,03230,Mexico DF"),
				Colaborador.create("Jhon", "Jhon_M", "Coordinador", "Jhon_M@ids.com.mx", 
						"5518294020", "22637868", "Ids Insurgentes", "4", "Av insurgentes sur 1318,03230,Mexico DF")
				);
	}

	public List<Colaborador> findAll() {
		return(repository.findAll() );
	}
	
	public void findById(Long ide) {
		System.out.println(repository.findById(ide) );
	}
	public void deleteById(Long ide) {
		repository.deleteById(ide) ;
		System.out.println(repository.findAll() );
	}
		
	public void deleteAll() {
		repository.deleteAll();
		System.out.println(repository.findAll() );
	}
	public void existsById(Long ide) {
		System.out.println(repository.existsById(ide));
	}
	
	public void update(String name, String alias, String title, String email, 
			String mobile,String phone, String office, String department_id, String w_a) {
		Colaborador c=Colaborador.create(name,alias,title,email, 
				mobile,phone,office,department_id,w_a);
		repository.saveAndFlush(c);
		System.out.println(repository.findAll() );
	}

}
