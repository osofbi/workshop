package mx.com.ids.directory.negocio;

import java.util.ArrayList;
import java.util.List;

public class Area {
	private String department_id;
	private List<Colaborador> colaboradores=new ArrayList<Colaborador>();
	private List<Area> departments=new ArrayList<Area>();
	
	public String getDepartment_id() {
		return department_id;
	}
	public void setDepartment_id(String department_id) {
		this.department_id = department_id;
	}
	public List<Colaborador> getColaboradores() {
		return colaboradores;
	}
	public void setColaboradores(List<Colaborador> colaboradores) {
		this.colaboradores = colaboradores;
	}
	public List<Area> getDepartments() {
		return departments;
	}
	public void setDepartments(List<Area> departments) {
		this.departments = departments;
	}
	@Override
	public String toString() {
		return "Area [department_id=" + department_id + ", colaboradores=" + colaboradores + ", departments="
				+ departments + "]";
	}
	public Area(String department_id, List<Colaborador> colaboradores, List<Area> departments) {
		super();
		this.department_id = department_id;
		this.colaboradores = colaboradores;
		this.departments = departments;
	}
	public Area() {
		super();
		this.department_id = "no tiene";
	}	
	
}
