DROP TABLE IF EXISTS Colaborador;
 
CREATE TABLE Colaborador (
  colaborador_id INT AUTO_INCREMENT  PRIMARY KEY,
  name VARCHAR(250) NOT NULL,
  alias VARCHAR(250) NOT NULL,
  title VARCHAR(250) DEFAULT NULL,
  email VARCHAR(250) DEFAULT NULL,
  mobile VARCHAR(250) DEFAULT NULL,
  phone VARCHAR(250) DEFAULT NULL,
  office VARCHAR(250) DEFAULT NULL,
  department_id VARCHAR(250) DEFAULT NULL,
  w_a VARCHAR(250) DEFAULT NULL


);
 