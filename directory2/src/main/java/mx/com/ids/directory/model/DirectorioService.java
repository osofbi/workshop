package mx.com.ids.directory.model;

import java.util.List;

public interface DirectorioService {
	public List<Colaborador> getAll();
	public void insert(Colaborador c);
}
