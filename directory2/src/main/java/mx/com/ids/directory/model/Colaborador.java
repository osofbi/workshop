package mx.com.ids.directory.model;

import java.util.ArrayList;
import java.util.List;

public class Colaborador {
	private String name;
	private String alias;
	private String title;
	private String email;
	private String mobile;
	private String phone;
	private String office;
	private String department_id;
	private String w_a;
	private Colaborador jefe_principal= new Colaborador();
	private List<Colaborador> jefes=new ArrayList<Colaborador>();
	private List<Colaborador> subalternos=new ArrayList<Colaborador>();
	
	
	
	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getAlias() {
		return alias;
	}



	public void setAlias(String alias) {
		this.alias = alias;
	}



	public String getTitle() {
		return title;
	}



	public void setTitle(String title) {
		this.title = title;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public String getMobile() {
		return mobile;
	}



	public void setMobile(String mobile) {
		this.mobile = mobile;
	}



	public String getPhone() {
		return phone;
	}



	public void setPhone(String phone) {
		this.phone = phone;
	}



	public String getOffice() {
		return office;
	}



	public void setOffice(String office) {
		this.office = office;
	}



	public String getDepartment_id() {
		return department_id;
	}



	public void setDepartment_id(String department_id) {
		this.department_id = department_id;
	}



	public String getW_a() {
		return w_a;
	}



	public void setW_a(String w_a) {
		this.w_a = w_a;
	}



	public Colaborador getJefe_principal() {
		return jefe_principal;
	}



	public void setJefe_principal(Colaborador jefe_principal) {
		this.jefe_principal = jefe_principal;
	}



	public List<Colaborador> getJefes() {
		return jefes;
	}



	public void setJefes(List<Colaborador> jefes) {
		this.jefes = jefes;
	}



	public List<Colaborador> getSubalternos() {
		return subalternos;
	}



	public void setSubalternos(List<Colaborador> subalternos) {
		this.subalternos = subalternos;
	}



	@Override
	public String toString() {
		return "Colaborador [name=" + name + ", alias=" + alias + ", title=" + title + ", email=" + email + ", mobile="
				+ mobile + ", phone=" + phone + ", office=" + office + ", department_id=" + department_id + ", w_a="
				+ w_a + ", jefe_principal=" + jefe_principal + ", jefes=" + jefes + ", subalternos=" + subalternos
				+ "]";
	}



	public Colaborador(String name, String alias, String title, String email, String mobile, String phone,
			String office, String department_id, String w_a, Colaborador jefe_principal, List<Colaborador> jefes,
			List<Colaborador> subalternos) {
		super();
		this.name = name;
		this.alias = alias;
		this.title = title;
		this.email = email;
		this.mobile = mobile;
		this.phone = phone;
		this.office = office;
		this.department_id = department_id;
		this.w_a = w_a;
		this.jefe_principal = jefe_principal;
		this.jefes = jefes;
		this.subalternos = subalternos;
	}



	public Colaborador() {
		super();
		this.name = "no tiene";
		this.alias = "no tiene";
		this.title = "no tiene";
		this.email = "no tiene";
		this.mobile = "no tiene";
		this.phone = "no tiene";
		this.office = "no tiene";
		this.department_id = "no tiene";
		this.w_a = "no tiene";
	}


}
