package mx.com.ids.directory.service;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Alta_Colaborador_Service {

	@RequestMapping ("/alta")
	public String saluda(String name,String alias,String title,String email,
			String mobile,String phone,String office,String department_id,String w_a) {

		return "Se dio de alta Colaborador [name=" + name + ", alias=" + alias + ", title=" + title + ", email=" + email + ", mobile="
				+ mobile + ", phone=" + phone + ", office=" + office + ", department=" + department_id + ", w_a=" + w_a
				+ "]";
	}

}