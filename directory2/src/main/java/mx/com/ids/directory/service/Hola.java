package mx.com.ids.directory.service;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Hola {

	@RequestMapping ("/saluda")
	public String saluda(String nombre) {
		
//		Servicio s = new MiClase();
//		
//		List<Contacto> resultado = s.busca("directores");
//		return resultado;
		return "Hola " + nombre + "!";
	}

}