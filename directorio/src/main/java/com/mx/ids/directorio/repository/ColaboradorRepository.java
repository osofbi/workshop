package com.mx.ids.directorio.repository;

import org.springframework.data.repository.CrudRepository;

import com.mx.ids.directorio.model.Colaborador;

public interface ColaboradorRepository extends CrudRepository<Colaborador, Integer> {

}
