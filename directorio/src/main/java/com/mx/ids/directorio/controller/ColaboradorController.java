package com.mx.ids.directorio.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mx.ids.directorio.model.Colaborador;
import com.mx.ids.directorio.service.ColaboradorService;

@RestController

public class ColaboradorController {

	@Autowired
	ColaboradorService colaboradorService;

	@GetMapping("/colaborador")
	private List<Colaborador> getAllColaborador() {
		return colaboradorService.getAllColaborador();
	}

	@GetMapping("/colaborador/lugar/{lugar}")
	private List<Colaborador> getColaboradorByPlace(@PathVariable("lugar") String lugar) {
		return colaboradorService.getColaboradorByPlace(lugar);
	}
	
	
	@GetMapping("/colaborador/area/{area}")  
	private List<Colaborador> getColaboradorByArea(@PathVariable("area") String area)   
	{  
	return colaboradorService.getColaboradorByArea(area);  
	}  

	@GetMapping("/colaborador/{id}")
	private Colaborador getColaborador(@PathVariable("id") int id) {
		return colaboradorService.getColaboradorById(id);
	}

	@DeleteMapping("/colaborador/{id}")
	private void deleteColaborador(@PathVariable("id") int id) {
		colaboradorService.delete(id);
	}

	@PostMapping("/colaborador")
	private int saveColaborador(@RequestBody Colaborador colaborador) {
		colaboradorService.saveOrUpdate(colaborador);
		return colaborador.getColaborador_id();
	}
}