package com.mx.ids.directorio.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity  
@Table  
public class Colaborador {

	@Id
	@Column  
	@GeneratedValue
	private int colaborador_id;
	@Column  
	private String name;
	@Column
	private String alias;
	@Column
	private String title;
	@Column
	private String email;
	@Column
	private String mobile;
	@Column
	private String phone;
	@Column
	private String office;
	@Column
	private String department;
	@Column
	private String w_a;
	//private Colaborador jefe_principal= new Colaborador();
	//private List<Colaborador> jefes=new ArrayList<Colaborador>();
	//private List<Colaborador> subalternos=new ArrayList<Colaborador>();
	
	
	public static Colaborador create(int colaborador_id, String name, String alias, String title, String email, 
			String mobile,String phone, String office, String department_id, String w_a) {
		Colaborador c = new Colaborador();
		c.setAlias(alias);
		c.setColaborador_id(colaborador_id);
		c.setDepartment(department_id);
		c.setEmail(email);
		c.setMobile(mobile);
		c.setName(name);
		c.setOffice(office);
		c.setPhone(phone);
		c.setTitle(title);
		c.setW_a(w_a);
		return c;
	}
	
	public static Colaborador create( String name, String alias, String title, String email, 
			String mobile,String phone, String office, String department_id, String w_a) {
		Colaborador c = new Colaborador();
		c.setAlias(alias);
		c.setDepartment(department_id);
		c.setEmail(email);
		c.setMobile(mobile);
		c.setName(name);
		c.setOffice(office);
		c.setPhone(phone);
		c.setTitle(title);
		c.setW_a(w_a);
		return c;
	}

	public int getColaborador_id() {
		return colaborador_id;
	}

	public void setColaborador_id(int colaborador_id) {
		this.colaborador_id = colaborador_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getOffice() {
		return office;
	}

	public void setOffice(String office) {
		this.office = office;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getW_a() {
		return w_a;
	}

	public void setW_a(String w_a) {
		this.w_a = w_a;
	}

	/*public Colaborador getJefe_principal() {
		return jefe_principal;
	}

	public void setJefe_principal(Colaborador jefe_principal) {
		this.jefe_principal = jefe_principal;
	}*/
	
	

	@Override
	public String toString() {
		return "Colaborador [colaborador_id=" + colaborador_id + ", name=" + name + ", alias=" + alias + ", title="
				+ title + ", email=" + email + ", mobile=" + mobile + ", phone=" + phone + ", office=" + office
				+ ", department=" + department + ", w_a=" + w_a + "]";
	}
	

	public Colaborador(int colaborador_id, String name, String alias, String title, String email, String mobile,
			String phone, String office, String department, String w_a) {
		super();
		this.colaborador_id = colaborador_id;
		this.name = name;
		this.alias = alias;
		this.title = title;
		this.email = email;
		this.mobile = mobile;
		this.phone = phone;
		this.office = office;
		this.department = department;
		this.w_a = w_a;
		//this.jefe_principal = jefe_principal;
	}
	public Colaborador() {}


}

