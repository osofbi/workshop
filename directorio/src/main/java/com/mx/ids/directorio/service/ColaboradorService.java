package com.mx.ids.directorio.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.ids.directorio.model.Colaborador;
import com.mx.ids.directorio.repository.ColaboradorRepository;

@Service
public class ColaboradorService {
	@Autowired
	ColaboradorRepository colaboradorRepository;

	public List<Colaborador> getAllColaborador() {
		List<Colaborador> colaboradores = new ArrayList<Colaborador>();
		colaboradorRepository.findAll().forEach(colaborador -> colaboradores.add(colaborador));
		return colaboradores;
	}

	// busca por lugar de trabajo
	public List<Colaborador> getColaboradorByPlace(String lugar) {
		List<Colaborador> todos = new ArrayList<Colaborador>();
		List<Colaborador> filtrados = new ArrayList<Colaborador>();
		colaboradorRepository.findAll().forEach(colaborador -> todos.add(colaborador));
		for (Colaborador c : todos) {
			if (lugar.equals(c.getW_a())) {
				filtrados.add(c);
			}
		}
		return filtrados;
	}
	
	// busca por area
	public List<Colaborador> getColaboradorByArea(String area) {
		List<Colaborador> todos = new ArrayList<Colaborador>();
		List<Colaborador> filtrados = new ArrayList<Colaborador>();
		colaboradorRepository.findAll().forEach(colaborador -> todos.add(colaborador));
		for (Colaborador c : todos) {
			if (area.equals(c.getDepartment())) {
				filtrados.add(c);
			}
		}
		return filtrados;
	}

	// busca por id
	public Colaborador getColaboradorById(int id) {
		return colaboradorRepository.findById(id).get();
	}

	// da de alta o actualiza si le das el id
	public void saveOrUpdate(Colaborador colaborador) {
		colaboradorRepository.save(colaborador);
	}

	// elimina
	public void delete(int id) {
		colaboradorRepository.deleteById(id);
	}
}
